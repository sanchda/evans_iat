<html>
<head>
	<title>Evans IAT</title>
</head>
<body>
<p>
	<?php
	session_start();
	
	echo '<h2>Implicit Association Test</h2><br>' . PHP_EOL;
	// Get a list of the test folders in the IAT_tests directory, which is
	// assumed to be two levels higher than this file.  Accumulate onto an
	// array for this purpose.	
	$rootDir = "IAT_tests/";
	$d = dir($rootDir);
	$testDirs = array();
	while (false !== ($entry = $d->read())) {
		if( is_dir($rootDir . $entry ) && strcmp(trim($entry),".") !== 0 && strcmp(trim($entry), ".." ) !== 0) {
			$testDirs[] = $entry;
		}
	}
	$d->close();
	
	// For each directory, check to make sure it contains:
	// - Legal test configuration script
	// - At least one subfolder (sequence)
	// - A valid configuration file
	// - A valid sequence configuration file for each sequence
	// - At least one image for each sequence class (divided into class1 and class2 directories)
	// - That the above data is confirmed by SQL database
	
	// Make array holding valid test information
	$testArray = array();
	$testArrray['names'] = array();
	
	// Iterate through the tests and display only the valid ones.
	foreach( $testDirs as &$this_testDir ) {
		$testConfig = file_get_contents($rootDir . $this_testDir . "/test.cfg");
		
		// Check that the configuration file exists
		if( false == file_exists( $rootDir . $this_testDir . "/test.cfg" ) ) {
			break;
		}
		// Check that the configuration file has the required components.
		$testConfig_Lines = preg_split("/\r\n|\n|\r/", $testConfig);
		
		// Crude filter (prevent locks if config.cfg has lots of newlines)
		// TODO:  check without exploding string
		//
		// The philosophy here is that I want to enable users to write comments
		// in their configuration files, with some arbitrary size limit to make
		// everything play nice with PHP limits. (which I don't want to mess with).
		if( count($testConfig_Lines) < 3 || count($testConfig_Lines) > 1000) {
			break;
		}
		
		// nan out these by default, so their status can be checked for set.
		// Weakly languages can be nice, I guess.  :)
		$testConfig_name = false;
		$testConfig_desc = false;
		$testConfig_order= false;
		
		foreach( $testConfig_Lines as &$thisLine ) {
			$thisLine_Parts = explode(":", $thisLine);
			if( count($thisLine_Parts) < 2 ) {
				// Not enough parts, which means no colon in line.  Escape,
				// but only one level out, since this line may be a comment.
				break;
			}
			
			// Everything after the first colon might be a valid part of a description
			// or something, so keep it.
			$thisLine_Tag   = trim($thisLine_Parts[0]);
			$thisLine_Desc  = trim(implode( "", array_slice($thisLine_Parts, 1) ));

			switch ($thisLine_Tag) {
				case "name":
					$testConfig_name = $thisLine_Desc;
					break;
				case "description":
					$testConfig_desc = $thisLine_Desc;
					break;
				case "sequence_order":
					$testConfig_order= str_replace(' ', '', $thisLine_Desc);
					break;
			}
		}
		
		// If none of the fields are false, then the config doesn't fail
		if( false == $testConfig_name || false == $testConfig_desc || false == testConfig_order ) {
			break;
		}
		
		// If every element of $testConfig_order is a valid sequence,
		// then the config doesn't fail.
		// For a sequence to be valid, it must:
		// - Exist
		// - Contain a properly-formatted configuration file
		// - Contain two folders named class1 and class2
		// - Each folder contains at least one image file
		$this_testDir_Order = explode(",", $testConfig_order);
		foreach( $this_testDir_Order as &$thisSequence ) {
			// Check that sequence exists
			if( !file_exists( $rootDir . $this_testDir . "/" . trim($thisSequence) ) ) {
				break 2;
			}
			
			// Check that it contains a configuration file
			$seqConfig = file_get_contents($rootDir . $this_testDir . "/" . trim($thisSequence) . "/sequence.cfg");
			if( !file_exists( $rootDir . $this_testDir . "/" . trim($thisSequence) . "/sequence.cfg" ) ) {
				break 2;
			}
			
			// Check that the configuration file has all required fields
			$sequenceConfig_Lines = preg_split("/\r\n|\n|\r/", $seqConfig);
			
			// Crude filter (prevent locks if sequence.cfg has lots of newlines)
			// TODO:  check without exploding string
			//
			// The philosophy here is that I want to enable users to write comments
			// in their configuration files, with some arbitrary size limit to make
			// everything play nice with PHP limits. (which I don't want to mess with).
			//
			// Also note:  number of lines may be variable, since I don't want to force
			// users to input a blank wordbanks (although they can).
			if( count($sequenceConfig_Lines) < 4 || count($sequenceConfig_Lines) > 1000) {
				break;
			}
			
			$sequenceConfig_preamble= false;
			$sequenceConfig_words11 = false;
			$sequenceConfig_words12 = false;
			$sequenceConfig_words21 = false;
			$sequenceConfig_words22 = false;
			$sequenceConfig_nClass1 = false;
			$sequenceConfig_nClass2 = false;
			$sequenceConfig_randImg = false;
			$sequenceConfig_randWrd = false;
			$sequenceConfig_rplcImg = false;
			$sequenceConfig_rplcWrd = false;
			
			// Iterate through lines of sequence config, writing down descriptions.
			foreach( $sequenceConfig_Lines as &$thisSequenceLine ) {
				$thisSequenceLine_Parts = explode(":", $thisSequenceLine);
				if( count($thisSequenceLine_Parts) < 2 ) {
					// Not enough parts, which means no colon in line.  Escape,
					// but only one level out, since this line may be a comment.
					break;
				}
				
				// Everything after the first colon might be a valid part of a description
				// or something, so keep it.
				$thisSequenceLine_Tag   = trim($thisSequenceLine_Parts[0]);
				$thisSequenceLine_Desc  = trim(implode( "", array_slice($thisSequenceLine_Parts, 1) ));
				
				switch ($thisSequenceLine_Tag) {
					case "preamble":
						$sequenceConfig_preamble= $thisSequenceLine_Desc;
						break;
					case "wordbank1_class1":
						$sequenceConfig_words11 = $thisSequenceLine_Desc;
						break;
					case "wordbank2_class1":
						$sequenceConfig_words21 = $thisSequenceLine_Desc;
						break;
					case "wordbank1_class2":
						$sequenceConfig_words12 = $thisSequenceLine_Desc;
						break;
					case "wordbank2_class2":
						$sequenceConfig_words22 = $thisSequenceLine_Desc;
						break;
					case "nClass1":
						$sequenceConfig_nClass1 = $thisSequenceLine_Desc;
						break;
					case "nClass2":
						$sequenceConfig_nClass2 = $thisSequenceLine_Desc;
						break;
					case "random_images":
						$sequenceConfig_randImg= $thisSequenceLine_Desc;
						break;
					case "random_wordbank":
						$sequenceConfig_randWrd= $thisSequenceLine_Desc;
						break;
					case "pick_with_replacement_images":
						$sequenceConfig_rplcImg= $thisSequenceLine_Desc;
						break;
					case "pick_with_replacement_wordbank":
						$sequenceConfig_rplcWrd= $thisSequenceLine_Desc;
						break;
				}
			}
			
			// If none of the fields are false, then the config doesn't fail
			if( false == $sequenceConfig_preamble||
				false == $sequenceConfig_nClass1 ||
				false == $sequenceConfig_nClass2 ) {
				break;
			}
			
			// Check that the class subfolders exist and have at least one image
			if( !file_exists( $rootDir . $this_testDir . "/" . trim($thisSequence) . "/class1" ) ) {
				break 2;
			}
			// Case-insensitive match for png, jpeg, jpg, gif, tiff, and tif.
			$classImages = glob( $rootDir . $this_testDir . "/" . trim($thisSequence) . "/class1/*.{[Pp][Nn][Gg],[Jj][Pp][Ee][Gg],[Jj][Pp][Gg],[Gg][Ii][Ff],[Tt][Ii][Ff][Ff],[Tt][Ii][Ff],[Bb][Mm][Pp]}",GLOB_BRACE);
			if( count($classImages) < 1 ) {
				break;
			}
		
			if( !file_exists( $rootDir . $this_testDir . "/" . trim($thisSequence) . "/class2" ) ) {
				break 2;
			}
			// Case-insensitive match for png, jpeg, jpg, gif, tiff, and tif.
			$classImages = glob( $rootDir . $this_testDir . "/" . trim($thisSequence) . "/class2/*.{[Pp][Nn][Gg],[Jj][Pp][Ee][Gg],[Jj][Pp][Gg],[Gg][Ii][Ff],[Tt][Ii][Ff][Ff],[Tt][Ii][Ff],[Bb][Mm][Pp]}",GLOB_BRACE);
			if( count($classImages) < 1 ) {
				break;
			}
		
			// Everything else checks out; this sequence was not determined to be invalid.
			// Check to see if the test array contains this test, if not make the key.
			// Either way, push this sequence to it.
			// ASSERT:
			// - Names of valid tests are unique
			if( !array_key_exists($testConfig_name, $testArray) ) {
				$testArray[$testConfig_name] = array();
				$testArray['names'][] = $testConfig_name;
				$testArray[$testConfig_name]['name'] = $testConfig_name;
				$testArray[$testConfig_name]['desc'] = $testConfig_desc;
				$testArray[$testConfig_name]['order']= $testConfig_order;
				$testArray[$testConfig_name]['testDir'] = $this_testDir;
				$testArray[$testConfig_name]['sequences']= array();
			}
				
			$testArray[$testConfig_name][$thisSequence] = array();
			$testArray[$testConfig_name]['sequences'][] = $thisSequence;
			$testArray[$testConfig_name][$thisSequence]['preamble'] = $sequenceConfig_preamble;
			$testArray[$testConfig_name][$thisSequence]['words11'] = $sequenceConfig_words11;
			$testArray[$testConfig_name][$thisSequence]['words12'] = $sequenceConfig_words12;
			$testArray[$testConfig_name][$thisSequence]['words21'] = $sequenceConfig_words21;
			$testArray[$testConfig_name][$thisSequence]['words22'] = $sequenceConfig_words22;
			$testArray[$testConfig_name][$thisSequence]['nClass1'] = $sequenceConfig_nClass1;
			$testArray[$testConfig_name][$thisSequence]['nClass2'] = $sequenceConfig_nClass2;
			$testArray[$testConfig_name][$thisSequence]['randImg'] = $sequenceConfig_randImg;
			$testArray[$testConfig_name][$thisSequence]['randWrd'] = $sequenceConfig_randWrd;
			$testArray[$testConfig_name][$thisSequence]['rplcImg'] = $sequenceConfig_rplcImg;
			$testArray[$testConfig_name][$thisSequence]['rplcWrd'] = $sequenceConfig_rplcWrd;
			
		}
		
		// Went through all the sequences.  Now do a little check to make sure that no
		// sequence failed, and remove the corresponding key from $testArray if it has.
		// If the key for this test exists and its available sequences is the same size
		// as the number of tokens in its order, then it's good.
		if( array_key_exists($testConfig_name, $testArray) ) {
			$nSequences = count( $testArray[$testConfig_name]['sequences'] );
			$nTokens    = count( explode(",", $testArray[$testConfig_name]['order']) );
			if( $nSequences !== $nTokens ) {
				unset($testArray[$testConfig_name]);
			}
		}
		
	}
	
	// Okay, all done.  $testArray should contain a nicely formatted summary of test parameters.
	// Pass array to session
	$_SESSION['testArray'] = $testArray;
	session_write_close();
	
	// Summarize the available tests now for the user.
	echo '<table class="core" width="840" cellpadding="10" cellspacing="0" border="0">';
	echo '<tr><td align="left"></td><td>&nbsp;</td></tr>';
	
	foreach( $testArray['names'] as &$thisTest ) {
		echo '<tr><td align="left" valign="top"><div class="back"><div class="section">';
		echo '<table width="186" cellpadding="0" cellspacing="0" border="0">';
		echo '<tr><td align="center" >';
		echo '<strong> <a href="preload_images.php?test=';
		//link
		echo htmlentities($thisTest);
		echo '">';
		//name
		echo $thisTest;
		echo '</a></strong></td></tr></table></div></div></td><td align="left" valign="top"><div class="text">';
		//desc
		echo $testArray[$thisTest]['desc'];
		echo '</div></td></tr>';
		
	}
	
	echo '</table>';
	
	?>
</p>
	</body>
</html>