<html>
<?php
	// Get session variables
	session_start();
	$testArray = $_SESSION['testArray'];
	$thisTest  = html_entity_decode($_GET['test']);
	$testArray = $testArray[$thisTest];  // Note that this makes the local version of testArray look different from what was passed to it!
	$testDir   = $testArray['testDir'];
	$rootDir = "IAT_tests/";
	
	// Determine number of sequences
	$testSequences = $testArray['sequences'];
	$nSequences    = count($testSequences);
	
	// Make the arrays for each sequence
	$seqArrays = array();
	foreach( $testSequences as &$thisSequence ) {
	// Get image paths for each class.  Note that these should be preloaded
	// by the time they are requested for display.
		$imgDir = $rootDir . $testDir . "/" . trim($thisSequence) . "/";
		$seqArray[$thisSequence]['class1_images'] = glob( $imgDir . "class1/*.{[Pp][Nn][Gg],[Jj][Pp][Ee][Gg],[Jj][Pp][Gg],[Gg][Ii][Ff],[Tt][Ii][Ff][Ff],[Tt][Ii][Ff],[Bb][Mm][Pp]}",GLOB_BRACE);
		$seqArray[$thisSequence]['class2_images'] = glob( $imgDir . "class2/*.{[Pp][Nn][Gg],[Jj][Pp][Ee][Gg],[Jj][Pp][Gg],[Gg][Ii][Ff],[Tt][Ii][Ff][Ff],[Tt][Ii][Ff],[Bb][Mm][Pp]}",GLOB_BRACE);

		// Process wordbanks
		// Each wordbank is a comma-delimited list, which may have whitespace in the lead or end.
		// Also, index.php could pass the second lists as false (instead of a string), in which case
		// the empty string should always be used.
		// If words1* are false, an error should have been raised in index.php.  Suppress here and just perform
		// the default behavior.
		if( false == $testArray[$thisSequence]['words11'] ) $testArray[$thisSequence]['words11'] = '';
		if( false == $testArray[$thisSequence]['words12'] ) $testArray[$thisSequence]['words12'] = '';
		if( false == $testArray[$thisSequence]['words21'] ) $testArray[$thisSequence]['words21'] = '';
		if( false == $testArray[$thisSequence]['words22'] ) $testArray[$thisSequence]['words22'] = '';
		
		$wordBank11 = array_map('trim', explode(',', $testArray[$thisSequence]['words11']));
		$wordBank12 = array_map('trim', explode(',', $testArray[$thisSequence]['words12']));
		$wordBank21 = array_map('trim', explode(',', $testArray[$thisSequence]['words21']));
		$wordBank22 = array_map('trim', explode(',', $testArray[$thisSequence]['words22']));
		
		// Each test has a specified number of images from each class.
		$class1Array = array_fill(0, ($testArray[$thisSequence]['nClass1']-1), 1 );
		$class2Array = array_fill(0, ($testArray[$thisSequence]['nClass2']-1), 2 );
		$classArray  = array_merge($class1Array, $class2Array);
		shuffle($classArray);
		$class1Buff = $seqArray[$thisSequence]['class1_images'];
		$class2Buff = $seqArray[$thisSequence]['class2_images'];
		shuffle($class1Buff);
		shuffle($class2Buff);
		
		// Iterate through, adding words randomly and images without replacement (until exhausted).
		$i = 0;
		foreach( $classArray as &$thisFrame ) {
			// Shuffle word banks
			// PHP's random array element code seems slower and nonuniform
			shuffle($wordBank11);
			shuffle($wordBank12);
			shuffle($wordBank21);
			shuffle($wordBank22);
			
			$seqArray[$thisSequence]['word1Top'][$i] = $wordBank11[0];
			$seqArray[$thisSequence]['word1Bot'][$i] = $wordBank21[0];
			$seqArray[$thisSequence]['word2Top'][$i] = $wordBank12[0];
			$seqArray[$thisSequence]['word2Bot'][$i] = $wordBank22[0];
			
			if( $thisFrame == 1 ) {
				// Check that this image class isn't exhausted--if so, refresh
				if( empty($class1Buff) ) {
					$class1Buff = $seqArray[$thisSequence]['class1_images'];
					shuffle($class1Buff);
				}
				$seqArray[$thisSequence]['image'][$i] = array_pop( $class1Buff );
				$seqArray[$thisSequence]['class'][$i] = 1;

			}
			
			else {
				if( empty($class1Buff) ) {
					$class2Buff = $seqArray[$thisSequence]['class2_images'];
					shuffle($class2Buff);
				}
				$seqArray[$thisSequence]['image'][$i] = array_pop( $class2Buff );
				$seqArray[$thisSequence]['class'][$i] = 2;
			}
			
			// Done populating classes
			$i++;
		}
	}
	
	// Make an images array
	$seqArray['images'] = array();	
	foreach( $testSequences as &$thisSequence ) {
		$seqArray['images'] = array_merge($seqArray['images'], $seqArray[$thisSequence]['image']);
		$seqArray['images'] = array_unique($seqArray['images']);
	}
?>


<head>
<?php
	echo '<title>' . $thisTest . ' preloader</title>';
?>
<script class="jsbin" src="imgPreloader.js"></script>
<script class="jsbin" src="jquery-1.11.1.min.js"></script>

<script>
	var seqArray = <?php echo json_encode($seqArray); ?>;
	var thisTestLink = <?php echo '\'takeIAT.php?test=' . $thisTest . '\''; ?>;
	
	function nextPage() {
		window.location.href = thisTestLink;
	}

	// Convert associative array to array
	var image_array = new Array();
	for (var images in seqArray['images']){
		image_array.push( seqArray['images'][images] );
}
	
	// instantiate the pre-loader with an onProgress and onComplete handler
	new preLoader(image_array, {
		
		onProgress: function(img, imageEl, index){
			// fires every time an image is done or errors. 
			// imageEl will be false if error
			console.log('just ' +  (!imageEl ? 'failed: ' : 'loaded: ') + img);
			
			// can access any property of this
			console.log(this.completed.length + this.errors.length + ' / ' + this.queue.length + ' done');
		}, 
		
		onComplete: function(loaded, errors){
			// fires when whole list is done. cache is primed.
			console.log('done', loaded);
			
			if (errors){
				console.log('the following failed', errors);
			}
			
			// Advance to the next page
			nextPage();
		}
	});
	

</script>
<body>
Preloading images; please wait.
</body>
</html>