function collapseElement(obj)
{
	var el = document.getElementById(obj);
	el.style.display = 'none';
}

function expandElement(obj)
{
	var el = document.getElementById(obj);
	el.style.display = '';
}

function collapsePages()
{
	var numFormPages = <?php echo (2*$nSequences + 1);?>;
	for(i=2; i <= numFormPages; i++)
	{
		currPageId = ('page_' + i);
		collapseElement(currPageId);
	}
}