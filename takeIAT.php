<html>
<?php

	// Filename: takeIAT.php
	// Author  : David Sanchez
	// Modified: 9/16/14
	//
	// This file manages IAT tests created from index.php, which
	// should accompany this file.  There are some assumptions:
	// - $_SESSION contains a uniqe testArray, which contains all
	//   of the data necessary to build the test
	// - preloadimags.php has been called.  This script contains a
	//   backup preloader, but the format is somewhat jarring for
	//   users, so preloadimages.php is better.
	//
	// We take a strange approach to displaying different test segments
	// one-by-one to the user.  First, a collapsePages() function is
	// defined, which manages which content is visible to the user.  Odd-
	// numbered pages contain preamble information (usually instructions),
	// whereas even-numbered pages contain the actual tests.  Methods for
	// viewing and updating pictures, timers, etc, are also provided.

	// Get session variables
	session_start();
	$testArray = $_SESSION['testArray'];
	$thisTest  = html_entity_decode($_GET['test']);
	$testArray = $testArray[$thisTest];  // Note that this makes the local version of testArray look different from what was passed to it!
	$testDir   = $testArray['testDir'];
	$rootDir = "IAT_tests/";
	
	// Determine number of sequences
	$testSequences = $testArray['sequences'];
	$nSequences    = count($testSequences);
	
	// Make the arrays for each sequence
	$seqArrays = array();
	foreach( $testSequences as &$thisSequence ) {
	// Get image paths for each class.  Note that these should be preloaded
	// by the time they are requested for display.
		$imgDir = $rootDir . $testDir . "/" . trim($thisSequence) . "/";
		$seqArray[$thisSequence]['class1_images'] = glob( $imgDir . "class1/*.{[Pp][Nn][Gg],[Jj][Pp][Ee][Gg],[Jj][Pp][Gg],[Gg][Ii][Ff],[Tt][Ii][Ff][Ff],[Tt][Ii][Ff],[Bb][Mm][Pp]}",GLOB_BRACE);
		$seqArray[$thisSequence]['class2_images'] = glob( $imgDir . "class2/*.{[Pp][Nn][Gg],[Jj][Pp][Ee][Gg],[Jj][Pp][Gg],[Gg][Ii][Ff],[Tt][Ii][Ff][Ff],[Tt][Ii][Ff],[Bb][Mm][Pp]}",GLOB_BRACE);

		// Process wordbanks
		// Each wordbank is a comma-delimited list, which may have whitespace in the lead or end.
		// Also, index.php could pass the second lists as false (instead of a string), in which case
		// the empty string should always be used.
		// If words1* are false, an error should have been raised in index.php.  Suppress here and just perform
		// the default behavior.
		if( false == $testArray[$thisSequence]['words11'] ) $testArray[$thisSequence]['words11'] = '';
		if( false == $testArray[$thisSequence]['words12'] ) $testArray[$thisSequence]['words12'] = '';
		if( false == $testArray[$thisSequence]['words21'] ) $testArray[$thisSequence]['words21'] = '';
		if( false == $testArray[$thisSequence]['words22'] ) $testArray[$thisSequence]['words22'] = '';
		
		$wordBank11 = array_map('trim', explode(',', $testArray[$thisSequence]['words11']));
		$wordBank12 = array_map('trim', explode(',', $testArray[$thisSequence]['words12']));
		$wordBank21 = array_map('trim', explode(',', $testArray[$thisSequence]['words21']));
		$wordBank22 = array_map('trim', explode(',', $testArray[$thisSequence]['words22']));
		
		// Each test has a specified number of images from each class.
		$class1Array = array_fill(0, ($testArray[$thisSequence]['nClass1']-1), 1 );
		$class2Array = array_fill(0, ($testArray[$thisSequence]['nClass2']-1), 2 );
		$classArray  = array_merge($class1Array, $class2Array);
		shuffle($classArray);
		$class1Buff = $seqArray[$thisSequence]['class1_images'];
		$class2Buff = $seqArray[$thisSequence]['class2_images'];
		shuffle($class1Buff);
		shuffle($class2Buff);
		
		// Iterate through, adding words randomly and images without replacement (until exhausted).
		$i = 0;
		foreach( $classArray as &$thisFrame ) {
			// Shuffle word banks
			// PHP's random array element code seems slower and nonuniform
			shuffle($wordBank11);
			shuffle($wordBank12);
			shuffle($wordBank21);
			shuffle($wordBank22);
			
			$seqArray[$thisSequence]['word1Top'][$i] = $wordBank11[0];
			$seqArray[$thisSequence]['word1Bot'][$i] = $wordBank21[0];
			$seqArray[$thisSequence]['word2Top'][$i] = $wordBank12[0];
			$seqArray[$thisSequence]['word2Bot'][$i] = $wordBank22[0];
			
			if( $thisFrame == 1 ) {
				// Check that this image class isn't exhausted--if so, refresh
				if( empty($class1Buff) ) {
					$class1Buff = $seqArray[$thisSequence]['class1_images'];
					shuffle($class1Buff);
				}
				$seqArray[$thisSequence]['image'][$i] = array_pop( $class1Buff );
				$seqArray[$thisSequence]['class'][$i] = 1;

			}
			else {
				if( empty($class2Buff) ) {
					$class2Buff = $seqArray[$thisSequence]['class2_images'];
					shuffle($class2Buff);
				}
				$seqArray[$thisSequence]['image'][$i] = array_pop( $class2Buff );
				$seqArray[$thisSequence]['class'][$i] = 2;
			}
			
			// Done populating classes
			$i++;
		}
	}
	
	// Make an images array
	$seqArray['images'] = array();	
	foreach( $testSequences as &$thisSequence ) {
		$seqArray['images'] = array_merge($seqArray['images'], $seqArray[$thisSequence]['image']);
		$seqArray['images'] = array_unique($seqArray['images']);
	}
?>
<head>
<?php
	echo '<title>' . $thisTest . '</title>';
?>

</head>
<body>
<div id="mainForm">

<?php
//	echo '<pre>';
//	print_r($seqArray);
//	echo '</pre>';

	// Iterate through the sequences, display
	$seqIterator = 1;
	
	foreach( $testSequences as &$thisSequence ) {
		// Format the preamble for the chosen sequence
		echo '<div id="page_' . $seqIterator . '" style="display: none;">' . PHP_EOL; 
		echo '<div>' . PHP_EOL;
		echo '<h2>' . $thisTest . '</h2>' . PHP_EOL;
		echo $testArray[$thisSequence]['preamble'] . PHP_EOL;
		echo '</div>' . PHP_EOL;
		echo '<div>' . PHP_EOL;
		echo '<input type=button onClick="collapseElement(\'page_' . $seqIterator . '\'); expandElement(\'page_2\');" value="Continue"/>' . PHP_EOL;
		echo '</div>' . PHP_EOL;
		echo '</div>' . PHP_EOL . PHP_EOL;
		$seqIterator++;
		
		// Prepare to show the sequence now.  Note the awkward regex to catch weird extensions...  Sorry.
		$imgDir = $rootDir . $testDir . "/" . trim($thisSequence) . "/";
		$class1Images = glob( $imgDir . "class1/*.{[Pp][Nn][Gg],[Jj][Pp][Ee][Gg],[Jj][Pp][Gg],[Gg][Ii][Ff],[Tt][Ii][Ff][Ff],[Tt][Ii][Ff],[Bb][Mm][Pp]}",GLOB_BRACE);
		$class2Images = glob( $imgDir . "class2/*.{[Pp][Nn][Gg],[Jj][Pp][Ee][Gg],[Jj][Pp][Gg],[Gg][Ii][Ff],[Tt][Ii][Ff][Ff],[Tt][Ii][Ff],[Bb][Mm][Pp]}",GLOB_BRACE);
	
		// Each frame in the image is defined by:
		// - An image
		// - Which class that image is in
		// - Which wordbank11 element is displayed on the left
		// - Which wordbank12 element is displayed on the left
		// - Which wordbank21 element is displayed on the right
		// - Which wordbank22 element is displayed on the right
		
		// Format the sequence itself
		// Put placeholders everywhere, since otherwise it'll goof up the
		// preloading.
		echo '<div id="page_' . $seqIterator . '" style="display: none;">' . PHP_EOL; 
		echo '<div>' . PHP_EOL;
		echo '<h2>' . $thisTest . '</h2>' . PHP_EOL;
		
		echo '<div id="seqContainer">' . PHP_EOL;
		echo '  <div id="txtContainer">' . PHP_EOL;
		echo '    <div id="leftContainer">' . PHP_EOL;
		echo '        <h3 id="leftTextTop">' .  '</h3>' . PHP_EOL;
		echo '        <h3 id="leftTextBot">' .  '</h3>' . PHP_EOL;
		echo '    </div>' . PHP_EOL;
		echo '    <div id="rightContainer">' . PHP_EOL;
		echo '        <h3 id="rightTextTop">' . '</h3>' . PHP_EOL;
		echo '        <h3 id="rightTextBot">' . '</h3>' . PHP_EOL;
		echo '    </div>' . PHP_EOL;
		echo '    <div id="textWrongContainer" class="tickFrame">' . PHP_EOL;
		echo '        <span class="helper"><h2 id="textWrong"><br></h2></span>'  . PHP_EOL;
		echo '    </div>' . PHP_EOL;
		echo '  </div>' . PHP_EOL;
		echo '  <div id="imgContainer" class="frame">' . PHP_EOL;
		echo '    <span class="helper"></span><img id=IATIMG src="placeholder.png"><br>' . PHP_EOL;
		echo '   </div>' . PHP_EOL;
		echo '	<div id="timerContainer">' . PHP_EOL;
// TODO
// See the section on innerHTML in IE for why the line below is here, even though it does nothing.
		echo '  <span id="iat_countdown" style="font-family:sans-serif;font-size:16pt"></span>' .  PHP_EOL;
		echo '  </div>'. PHP_EOL;
		echo '</div>' . PHP_EOL;

		echo '</div>' . PHP_EOL;
		echo '<div>' . PHP_EOL;
		echo '</div>' . PHP_EOL;
		echo '</div>' . PHP_EOL . PHP_EOL;
		$seqIterator++;
		
	}
	
	// Display a final page with a link home
	echo '<div id="page_' . $seqIterator . '" style="display: none;">' . PHP_EOL; 
	echo '<div style="display: inherit;">' . PHP_EOL;
	echo '<h2>' . $thisTest . '</h2>' . PHP_EOL;
	echo 'You have completed this IAT.  Please click <a href=index.php>here</a> to return to the menu.<br>' . PHP_EOL;
	echo '</div>' . PHP_EOL;
	echo '</div>' . PHP_EOL . PHP_EOL;
	
	echo '</div>' . PHP_EOL . PHP_EOL . PHP_EOL;
	
?>

<style>
ul {list-style-type:none;}

#seqContainer{
  width:500px;
  height:600px;
  position:relative;
  z-index:5;
  top: 100px;
  left:33%;
  margin:-100px 0 0 -150px;
}
div.seqContainer label{
  width:160px;
  display:block;
  float:left;
}
div.seqContainer .input{
  margin-left:4px;
  float:left;
}

#txtContainer{
  overflow:hidden;
  width:100%;
  z-index:8;
}

#leftContainer{
  overflow:hidden;
  width:50%;
  float:left;
  z-index:9;
}

#rightContainer{
  overflow:hidden;
  width:50%;
  float:right;
  text-align:right;
  z-index:9;
}

#textWrongContainer{
  overflow:hidden;
  width:100%;
  float:left;
  text-align:center;
  z-index:10;
}

#imgContainer{
  overflow:hidden;
  width:100%;
  float:left;
  z-index:10;
}

#timerContainer{
  overflow:hidden;
  width:100%;
  float:left;
  text-align:right;
  z-index:6;
}

#iat_countdown{
  overflow:hidden;
  width:100%;
  float:left;
  text-align:right;
  z-index:5;
}

.tickFrame {
    height: 50px;      
    width:  50px;
    white-space: nowrap;
    text-align: center; margin: 1em 0;
}

.frame {
    height: 400px;      /* equals max image height, minus a bit because IE */
    width:  400px;
    white-space: nowrap;
    text-align: center; margin: 1em 0;
}

.helper {
    display: inline-block;
    height: 100%;
    vertical-align: middle;
}

img {
    background: #3A6F9A;
    vertical-align: middle;
    max-height: 350px;
}

  
</style>

<!--Pages function-->
<script class="jsbin" src="shortcut.js"></script>
<script type=text/javascript>

	//  Define globals
	var thisSequence = "<?php echo $testSequences[0]; ?>";
	var seqFrame = 0;
	var thisSeqNum   = 0;
    var seqArray = <?php echo json_encode($seqArray); ?>;
	var testArray = <?php echo json_encode($testArray); ?>;
	var sequences = <?php echo json_encode($testSequences); ?>;
	var thisTest  = "<?php echo htmlentities($thisTest); ?>";
	
	// Ugly workaround to passing time to funcs below
	var slideStartTime = 0;
	var thisSlideTimer = 0;
	
	// A function for updating a countdown on page_2.  If the time runs out,
	// send the user back to the first page with a message that they've run
	// out of time.
	// This function is initialized in initSequence(), attached to a timer.
	function setCountdownTimer() {
		// Is the user done with the sequence?
		if( (seqFrame+1) > seqArray[thisSequence]['class'].length) {
			document.getElementById("iat_countdown").innerHTML = " ";
			return;
		}
		
		// Has the timer even been set?
		// TODO:  this is managed with globals.  Fix.
		if (slideStartTime == 0) {
			document.getElementById("iat_countdown").innerHTML = 4.00;
		}
	
		// Otherwise, no.  Compute time remaining
		var thisTime = new Date().getTime();
		var timeLeft = Math.round( (slideStartTime - thisTime + 4000)/10);
		timeLeft = timeLeft/100;
		
		
// TODO
// The commented sections here don't work on IE.  It implements innerHTML in a way that breaks divs.
// could try direct DOM insertion, but I already wrote this code in an extremely sloppy way and would
// recommend rewrite on that basis...		
		// If there is still time, show it.  Otherwise, reset the test and tell the user.
		if( timeLeft > 1 ) {
//			document.getElementById("iat_countdown").innerHTML = timeLeft;
//			document.getElementById("iat_countdown").style.color = 'black';
			clearTimeout(thisSlideTimer);
			thisSlideTimer = setTimeout( setCountdownTimer, 101);
		} else if( timeLeft > 0 && timeLeft < 1) {
//			document.getElementById("iat_countdown").innerHTML = timeLeft;
//			document.getElementById("iat_countdown").style.color = 'red';
			clearTimeout(thisSlideTimer);
			thisSlideTimer = setTimeout( setCountdownTimer, 101);
		} else {
			// reset timer; display out-of-time page
//			document.getElementById("iat_countdown").innerHTML = 0;
			gotoString = "outoftime.php?test=";
			gotoString = gotoString.concat(thisTest);
//			window.location.href = gotoString;

		}
	}
	
	// This initializes a test sequence, which is necessary for initializing
	// the timers.
	function initSequence() {
		slideStartTime = new Date().getTime();
		destroySequence(thisSlideTimer);
		thisSlideTimer = setTimeout( setCountdownTimer, 111);
	}
	
	function destroySequence(slideTimer) {
		clearTimeout(slideTimer);
	}
	
	function updateLeftTop(newText) {
		document.getElementById("leftTextTop").innerHTML = newText;
	}
	function updateLeftBot(newText) {
		document.getElementById("leftTextBot").innerHTML = newText;
	}
	function updateRightTop(newText) {
		document.getElementById("rightTextTop").innerHTML = newText;
	}
	function updateRightBot(newText) {
		document.getElementById("rightTextBot").innerHTML = newText;
	}
	
	function updateImg(newImage) {
		document.getElementById("IATIMG").src = newImage;
	}
	
	function setWrong() {
		document.getElementById("textWrong").innerHTML = '<p style="color:red;font-family:Helvetica;">X</p>';
	}
	
	function setRight() {
		clearWrong();
		updateElements();
//		storeTime();
//		resetTimer();
	}
	
	function clearWrong() {
		document.getElementById("textWrong").innerHTML = "<br>";
	}
	
	// Reuse the code from updateElements to initialize (just don't increment the frame)
	function initElements() {
		clearWrong();
		destroySequence(thisSlideTimer);
		if( thisSeqNum & 2 == 0 ) {
			thisSlideTimer = initSequence();
		}
		updateLeftTop(seqArray[thisSequence]['word1Top'][seqFrame]);
		updateLeftBot(seqArray[thisSequence]['word1Bot'][seqFrame]);
		updateRightTop(seqArray[thisSequence]['word2Top'][seqFrame]);
		updateRightBot(seqArray[thisSequence]['word2Bot'][seqFrame]);
		updateImg(seqArray[thisSequence]['image'][seqFrame]);
		clearWrong();
		
	}
	
	// Page movement functions
	function collapseElement(obj) {
		var el = document.getElementById(obj);
		el.style.display = 'none';
	}
	function expandElement(obj) {
		var el = document.getElementById(obj);
		el.style.display = 'inline';
		initElements();
	}
	function collapsePages() {
		var numFormPages = <?php echo (2*$nSequences + 1);?>;
		for(i=2; i <= numFormPages; i++){
			currPageId = ('page_' + i);
			collapseElement(currPageId);
		}
	}
	

	// Updates the elements, using the above.
	function updateElements() {
		clearWrong();
		seqFrame++;	
		destroySequence(thisSlideTimer);
		initSequence();
		updateLeftTop(seqArray[thisSequence]['word1Top'][seqFrame]);
		updateLeftBot(seqArray[thisSequence]['word1Bot'][seqFrame]);
		updateRightTop(seqArray[thisSequence]['word2Top'][seqFrame]);
		updateRightBot(seqArray[thisSequence]['word2Bot'][seqFrame]);
		updateImg(seqArray[thisSequence]['image'][seqFrame]);
		clearWrong();
		
		// What if we're done with the sequence?
		if( (seqFrame+1) > seqArray[thisSequence]['class'].length) {
			seqFrame = 0;
			thisSeqNum++;
			thisSequence = sequences[thisSeqNum];
			
			// destroy all timers for now
			// (no need to display during instructions/preamble)
			destroySequence(thisSlideTimer);
			
			// Display new page, which is always going to be offset.
			var pageString = 'page_';
			thisPageNum = 2*(thisSeqNum);
			nextPageNum = thisPageNum+1;
			thisPageID = pageString.concat( thisPageNum.toString() );
			nextPageID = pageString.concat( nextPageNum.toString() );
			
			// Page 2 is the default picture page...  Yup, I'm that stupid.
			// Fortunately, no reason to change it, since many of the functions here
			// aren't strictly aware of the page context anyway.
			collapseElement('page_2');
			expandElement(nextPageID);
			updateElements();
		}
	}
	
	// Submits a guess about the current image.
	// Uses the global variables seqArray, thisSequence, and seqFrame .
	// These latter
	function guessClass1() {
		if( seqArray[thisSequence]['class'][seqFrame] == 1 ) {
			setRight();
		}
		else {
			setWrong();
		}
	}
	
	function guessClass2() {
		if( seqArray[thisSequence]['class'][seqFrame] == 2 ) {
			clearWrong();
			updateElements();
		}
		else {
			setWrong();
		}
	}
	
	function initKeys() {
		shortcut.add("e", guessClass1 ,{"disable_in_input":true});
		shortcut.add("i", guessClass2 ,{"disable_in_input":true});

	}
 
	window.onload = new function() { initKeys(); };
	window.onload = new function() { collapsePages(); };
	window.onload = new function() { expandElement('page_1'); };


	</script>

</body>
</html>